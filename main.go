package main

import (
	"carCost/src/adapters/delivery/proto"
	"carCost/src/adapters/handlers"
	"carCost/src/core/services"
	"fmt"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

var (
	grpcHandler *handlers.GRPCHandler
	svc         *services.CarCostService
)

func init() {
	godotenv.Load()
}

func main() {
	svc = services.NewCarCostService()
	InitGRPC()
}

func InitGRPC() {
	s := grpc.NewServer()
	grpcHandler = handlers.NewGRPCHandler(*svc)
	proto.RegisterCarCostServiceServer(s, grpcHandler)

	grpcPort := os.Getenv("GRPC_PORT")
	grpcAddr := fmt.Sprintf(":%s", grpcPort)
	l, err := net.Listen("tcp", grpcAddr)
	if err != nil {
		log.Fatal(err)
	}

	if err := s.Serve(l); err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}
}
