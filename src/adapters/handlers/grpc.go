package handlers

import (
	"carCost/src/adapters/delivery/proto"
	"carCost/src/core/domain"
	"carCost/src/core/services"
	"context"
	"fmt"
)

type GRPCHandler struct {
	svc services.CarCostService
}

func NewGRPCHandler(CarCostService services.CarCostService) *GRPCHandler {
	return &GRPCHandler{
		svc: CarCostService,
	}
}

func (h *GRPCHandler) CarCost(ctx context.Context, req *proto.GetCarCostRequest) (*proto.GetCarCostResponse, error) {
	carData := req.GetCar()
	car := domain.Car{
		ProductionYear: int(carData.Year),
		Mileage:        int(carData.Mileage),
		CarType:        carData.Type,
	}
	fmt.Println(car)
	carCost := h.svc.GetCarCost(&car)

	return &proto.GetCarCostResponse{CarCost: carCost}, nil
}

func (h *GRPCHandler) mustEmbedUnimplementedCarCostServiceServer() {}
