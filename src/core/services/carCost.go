package services

import (
	"carCost/src/core/domain"
	"time"
)

type CarCostService struct {
}

func NewCarCostService() *CarCostService {
	return &CarCostService{}
}

func (c *CarCostService) getYearRate(year int) float32 {
	curYear := time.Now().Year()
	rate := 1.0 + float32(year/curYear)
	return rate
}

func (c *CarCostService) getMileageRate(mileage int) float32 {
	rate := 100000 / float32(mileage)
	return rate
}

func (c *CarCostService) getTypeRate(carType string) float32 {
	typeMap := map[string]float32{
		"taxi":     0.5,
		"delivery": 0.75,
		"car":      1.0,
	}
	rate := typeMap[carType]
	return rate
}

func (c *CarCostService) GetCarCost(car *domain.Car) float32 {
	yearRate := c.getYearRate(car.ProductionYear)
	mileageRate := c.getMileageRate(car.Mileage)
	typeRate := c.getTypeRate(car.CarType)

	carCost := (yearRate + mileageRate) * typeRate * 100

	return carCost
}
