package ports

import "carCost/src/core/domain"

type CarCostService interface {
	getYearRate(year int) float32
	getMileageRate(mileage int) float32
	getTypeRate(carType string) float32
	getCarCost(car *domain.Car, carType string)
}
