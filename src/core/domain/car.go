package domain

type Car struct {
	ProductionYear int
	Mileage        int
	CarType        string
}
